"""
Mapper class for Topex GDR Files
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from collections import OrderedDict
import datetime
import logging
import re
import struct

import numpy

from cerbere.datamodel.field import Field
from cerbere.datamodel.variable import Variable
from cerbere.mapper.abstractmapper import AbstractMapper, READ_ONLY


FIELDS = [
    'days', 'msecs', 'lat', 'lon', 'off_nadir_wf', 'off_nadir_pf', 'range',
    'range_std', 'swh', 'swh_c',  'swh_std', 'swh_std_c', 'swh_numval',
    'sig0', 'sig0_c', 'agc_rms', 'agc_numval', 'windsp_alt', 'bathy'
]       

QUALFIELDS = [
    'alton', 'instr_tp', 'imanv', 'lat_err', 'lon_err', 'att_pf', 'alt_bad1',
    'alt_bad2', 'geo_bad1', 'geo_bad2'
]

SCALE_FACTOR = {
    'lat': 1.e-6,
    'lon':  1.e-6,
    'windsp_alt': 1.e-3,
    'range': 1.e-3,
    'range_std': 1.e-3,
    'swh': 1.e-2,
    'swh_c': 1.e-2,
    'swh_std': 1.e-2,
    'swh_std_c': 1.e-2,
    'agc': 1.e-2,
    'sig0': 1.e-2,
    'sig0_c': 1.e-2,
    'off_nadir_pf': 1.e-2,
    'off_nadir_wf': 1.e-2,
    'bathy': 1.,
    'windsp_alt': 1.e-1,
}

UNITS = {
    'lat': 'degrees_north',
    'lon':  'degrees_east',
    'windsp_alt': 'm s-1',
    'windsp_mod_u': 'm s-1',
    'windsp_mod_v': 'm s-1',
    'range': 'm',
    'range_c': 'm',
    'range_std': 'm',
    'range_std_c': 'm',
    'swh': 'm',
    'swh_c': 'm',
    'swh_std': 'm',
    'swh_std_c': 'm',
    'sig0': 'dB',
    'sig0_c': 'dB',
    'off_nadir_pf': 'degree2',
    'off_nadir_wf': 'degree2',
    'bathy': 'm',

}

FILLVALUE = {
    'range': 2147483647,
    'range_std': 32767,
    'swh': 65535,
    'swh_c': 65535,
    'swh_numval': 127,
    'swh_std': 255,
    'swh_std_c': 255,
    'agc': 65535,
    'agc_numval': 127,
    'sig0': 65535,
    'sig0_c': 65535,
    'off_nadir_pf': 255,
    'off_nadir_wf': 255,
    'bathy': 32767,
    'windsp_alt': 255,
    'instr_tp': 255,
    'imanv': 127,
    'lat_err': 127,
    'lon_err': 127,
    'att_pf': 255,
}

DTYPE = {
    'lat': numpy.float64,
    'lon':  numpy.float64,
    'windsp_alt': numpy.float64,
    'range': numpy.float64,
    'range_std':numpy.float64,
    'swh': numpy.float64,
    'swh_c': numpy.float64,
    'swh_numval': numpy.uint8,
    'swh_std': numpy.float64,
    'swh_std_c': numpy.float64,
    'agc': numpy.float64,
    'agc_rms': numpy.float64,
    'agc_numval': numpy.uint8,
    'sig0': numpy.float64,
    'sig0_c': numpy.float64,
    'off_nadir_pf': numpy.float64,
    'off_nadir_wf': numpy.float64,
    'bathy': numpy.float64,
    'alton': numpy.uint8,
    'instr_tp': numpy.uint8,
    'imanv': numpy.uint8,
    'lat_err': numpy.uint8,
    'lon_err': numpy.uint8,
    'att_pf': numpy.uint8,
    'alt_bad1': numpy.uint8,
    'alt_bad2': numpy.uint8,
    'geo_bad1': numpy.uint8,
    'geo_bad2': numpy.uint8,
}



# b, h, i decode 1, 2, 4 byte integers respectively, capitals are
# UNSIGNED, nx ignores n bytes
RECORD_STRUCT = '<hi 14x ii 48x BBi 21x h 31x HHBBB 10x HH 4x H 8x b 17x B h 36x'
QUAL_STRUCT = '<198x bB 2x bbbb 6x BB 9x BB 3x'


class TPGDRFile(AbstractMapper):
    """Mapper for ESA Topex and Poseidon merged GDR files
    """

    def __init__(self, url=None, mode=READ_ONLY, alt=1, **kwargs):
        """
        Args:
            alt: altimeter (0=Poseidon, 1=Topex)
        """
        super(TPGDRFile, self).__init__(url=url, mode=mode, **kwargs)
        self.altimeter = alt

    def _read_global_attributes(self):
        self.attributes = OrderedDict([])

        # read header and extract information
        # PASS_FILE_HEADER size 7524
        header = self._handler.read(7524)
    
        atts = ('Generating_Software_Name', 'Build_Id')
        for label in atts:
            patn = '(' + label + ' = )(.*?)(;)'
            m = re.search(patn, header)
            if label == 'Generating_Software_Name':
                self.attributes['source'] = m.group(2)
            if label == 'Build_Id':
                self.attributes['software_version']  = m.group(2)
    
        atts = ('Cycle_Number', 'Pass_Number', 'Pass_Data_Count', 'Equator_Longitude')
        for label in atts:
            patn = '(' + label + ' = +)(\d+)(;|\.\d+)'
            m = re.search(patn, header)
            if label == 'Cycle_Number':
                self.attributes['cycle_number'] = m.group(2)
            if label == 'Pass_Number':
                self.attributes['pass_number'] = m.group(2)
            if label == 'Pass_Data_Count':
                ndata = m.group(2)
                self.count = int(ndata)
            if label == 'Equator_Longitude':
                lon = float(m.group(2) + m.group(3))
                self.attributes['equator_crossing_longitude'] = lon
            
        atts = ('Equator_Time', 'Time_First_Pt', 'Time_Last_Pt', 'Time_Epoch')
        for label in atts:
            patn = '(' + label + ' = )(\d+)(-)(\d+)(T)(\d+:\d+:\d+)(\.\d+)'
            m = re.search(patn, header)
            if label == 'Equator_Time':
                strtime = m.group(2) + ' ' + m.group(4) + ' ' + m.group(6)
                eqt = datetime.datetime.strptime(strtime, "%Y %j %H:%M:%S")
                self.attributes['equator_crossing_time'] = eqt
            if label == 'Time_First_Pt':
                strtime = m.group(2) + ' ' + m.group(4) + ' ' + m.group(6)
                tfp = datetime.datetime.strptime(strtime, "%Y %j %H:%M:%S")
                self.attributes['time_coverage_start'] = tfp
            if label == 'Time_Last_Pt':
                strtime = m.group(2) + ' ' + m.group(4) + ' ' + m.group(6)
                tfp = datetime.datetime.strptime(strtime, "%Y %j %H:%M:%S")
                self.attributes['time_coverage_end'] = tfp
            if label == 'Time_Epoch':
                strtime = m.group(2) + ' ' + m.group(4) + ' ' + m.group(6)
                tfp = datetime.datetime.strptime(strtime, "%Y %j %H:%M:%S")
                self.time_epoch = tfp


    def _read_data(self):

        # read input data records
        current = 0
        self.times = numpy.ma.masked_all((self.count,), dtype=float)
        self.data = {}

        while (current < self.count):
            record = self._handler.read(228)
            try:
                values = struct.unpack(RECORD_STRUCT, record)
                qvalues = struct.unpack(QUAL_STRUCT, record)

            except struct.error:
                logging.warning(
                    "Record length not as expected at record %i of %i" % (
                        current, self.count
                    )
                )
                raise

            # times
            self.times[current] = (
                values[0] * 86400. + values[1] / 1000.
            )
            # data
            for i, field in enumerate(FIELDS[2:]):
                if field not in self.data:
                    self.data[field] = numpy.ma.masked_all(
                        (self.count,), dtype=DTYPE[field]
                        )
                self.data[field][current] = values[i + 2]
            for i, field in enumerate(QUALFIELDS[:]):
                if field not in self.data:
                    self.data[field] = numpy.ma.masked_all(
                        (self.count,), dtype=DTYPE[field]
                        )
                self.data[field][current] = qvalues[i]

            current += 1
        
        # select topex measurements only
        topex = self.data['alton'] == 1
        self.times = self.times[topex]
        for field in self.data:
            self.data[field] = self.data[field][topex]
        self.count = len(self.times)

        # create field objects
        self._fields = {}
        dims = OrderedDict([('time', self.count)])
        self._fields['time'] = Field(
            variable=Variable('time', 'time'),
            dimensions=dims,
            values=self.times,
            datatype=self.times.dtype,
            units=self.time_epoch.strftime('seconds since %Y-%m-%d')
        )
        for f in FIELDS[2:] + QUALFIELDS:
            values = self.data[f]
            if f in FILLVALUE:
                values = numpy.ma.masked_equal(values, FILLVALUE[f], copy=False)

            if f in SCALE_FACTOR:
                print(f, SCALE_FACTOR[f], values.dtype)
                values *= SCALE_FACTOR[f]

            units = None
            if f in UNITS:
                units = UNITS[f]

            self._fields[f] = Field(
                variable=Variable(f, f),
                dimensions=dims,
                values=values,
                datatype=values.dtype,
                units=units
            )    

        # convert lon to -180/180
        ind = self.data['lon'] > 180
        self.data['lon'][ind] -= 360.
        
    def open(self, view=None, **kwargs):
        super(TPGDRFile, self).open(view=view)
        self._handler = open(self._url, 'r')
        self._read_global_attributes()
        self._read_data()

    def get_geolocation_field(self, fieldname):
        return u'time'

    def get_matching_dimname(self, dimname):
        return dimname

    def get_standard_dimname(self, dimname):
        return dimname

    def get_dimensions(self, **kwargs):
        return ('time',)

    def get_dimsize(self, dimname):
        if dimname != 'time':
            raise ValueError('Unknown dimension {}'.format(dimname))
        return len(self.times)

    def get_fieldnames(self):
        return FIELDS[4:] + QUALFIELDS

    def close(self):
        """Close handler on storage"""
        self._handler.close()

    def read_values(self, fieldname, slices=None, **kwargs):
        """Read the data of a field.

        Args:
            fieldname (str): name of the field which to read the data from

            slices (list of slice, optional): list of slices for the field if
                subsetting is requested. A slice must then be provided for each
                field dimension. The slices are relative to the opened view
                (see :func:open) if a view was set when opening the file.

        Return:
            MaskedArray: array of data read. Array type is the same as the
                storage type.
        """
        if fieldname == 'time':
            values = self.times
        else:
            values = self.data[fieldname]
        if slices is not None:
            values = values[slices]
        return values

    def read_field(self, fieldname):
        return self._fields[fieldname]

    def read_field_attributes(self, fieldname):
        return self._fields[fieldname].attributes

    def read_fillvalue(self, fieldname):
        return self.fields[fieldname].fill_value

    def get_start_time(self):
        return self.attributes['time_coverage_start']

    def get_end_time(self):
        return self.attributes['time_coverage_end']

    def read_global_attributes(self):
        return self.attributes.keys()

    def read_global_attribute(self, attr):
        return self.attributes[attr]

    def create_dim(self, **kwargs):
        raise NotImplementedError

    def create_field(self, **kwargs):
        raise NotImplementedError

    def write_field(self, **kwargs):
        raise NotImplementedError

    def write_global_attributes(self, **kwargs):
        raise NotImplementedError

    def get_bbox(self, **kwargs):
        pass


class TopexGDRFile(TPGDRFile):
    """Mapper for ESA Topex GDR files
    """

    def __init__(self, url=None, mode=READ_ONLY, **kwargs):
        """
        """
        super(TopexGDRFile, self).__init__(url=url, mode=mode, alt=1, **kwargs)

class PoseidonGDRFile(TPGDRFile):
    """Mapper for ESA Poseidon GDR files
    """

    def __init__(self, url=None, mode=READ_ONLY, **kwargs):
        """
        """
        super(PoseidonGDRFile, self).__init__(url=url, mode=mode, alt=0, **kwargs)


