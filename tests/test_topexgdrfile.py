"""

Test class for cerbere Topex GDR mapper

:copyright: Copyright 2016 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from cerbere.mapper.checker import Checker


class TopexGDRChecker(Checker, unittest.TestCase):
    """Test class for Topex GDR files"""

    def __init__(self, methodName="runTest"):
        super(TopexGDRChecker, self).__init__(methodName)

    @classmethod
    def mapper(cls):
        """Return the mapper class name"""
        return 'topexgdrfile.TopexGDRFile'

    @classmethod
    def datamodel(cls):
        """Return the related datamodel class name"""
        return 'Trajectory'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "MGC364.054"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"